stages:
  - build app
  - create key pair
  - create aws infrastructure
  - deploy elastic
  - destroy all

build-app:
  stage: build app
  image: maven:3-amazoncorretto-17
  script:
    - mvn clean
    - mvn install -Dmaven.test.skip=true
  artifacts:
    paths:
      - target/app-0.0.1-SNAPSHOT.jar

key-pair-job:
  stage: create key pair
  image: docker:latest
  script:
    - ssh-keygen -t rsa -b 2048 -f MyKey.pem -q -P ''
    - chmod 400 MyKey.pem
    - ssh-keygen -y -f MyKey.pem > MyKey.pub
  artifacts:
    paths:
      - "MyKey.pem"
      - "MyKey.pub"
      - target/app-0.0.1-SNAPSHOT.jar

create-aws-infra:
  stage: create aws infrastructure
  image: 
    name: hashicorp/terraform:latest
    entrypoint:
      - '/usr/bin/env'
      - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
  before_script:
    - export AWS_ACCESS_KEY=${AWS_ACCESS_KEY_ID}
    - export AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY} 
  script:
    - cd terraform
    - terraform init
    - terraform plan
    - terraform apply --auto-approve
    - pwd
    - ls -latr
  artifacts:
    paths:
      - terraform/terraform.tfstate
      - terraform/terraform.tfstate.backup
      - terraform/elastic-ip
      - terraform/kibana-ip
      - terraform/application-ip
      - "MyKey.pem"
      - "MyKey.pub"
      - target/app-0.0.1-SNAPSHOT.jar

prepare-ansible:
  image: ubuntu:18.04
  stage: deploy elastic   # It can run at the same time as unit-test-job (in parallel).
  script:
    
    - cd ansible
    - touch inventory.ini
    - echo "[application_host]" | tee -a inventory.ini
    - export application_host=$(cat ../terraform/application-ip)
    - echo "$application_host ansible_user=ubuntu ansible_ssh_private_key_file=../MyKey.pem" | tee -a inventory.ini
    - echo "[elastic_host]" | tee -a inventory.ini
    - export elastic_host=$(cat ../terraform/elastic-ip)
    - echo "$elastic_host ansible_user=ubuntu ansible_ssh_private_key_file=../MyKey.pem" | tee -a inventory.ini
    - echo "[kibana_host]" | tee -a inventory.ini
    - export kibana_host=$(cat ../terraform/kibana-ip)
    - echo "$kibana_host ansible_user=ubuntu ansible_ssh_private_key_file=../MyKey.pem" | tee -a inventory.ini
    - export elastic_host_url=" http://$elastic_host:9200"
    - export logstash_host_url=" $elastic_host:5044"
    - echo "elastic_url:$elastic_host_url" | tee -a group_vars/all
    - echo "logstash_host:$logstash_host_url" | tee -a group_vars/all
  artifacts:
    paths:
      - ansible/inventory.ini
      - ansible/group_vars/all
      - "MyKey.pem"
      - "MyKey.pub"
      - target/app-0.0.1-SNAPSHOT.jar

deploy-job:    
  stage: deploy elastic
  image: ubuntu:18.04
  script:
    - echo "Installing Ansible..."
    - apt update
    - apt install -y ansible && apt install -y ssh
    - export ANSIBLE_HOST_KEY_CHECKING=False
    - echo "Deploying Elastic and Kibana..."
    - ls -latr
    - cd ansible
    - ls -latr
    - ansible-playbook site.yml -i inventory.ini
    - echo "Application successfully deployed."
  needs: [prepare-ansible]

destroy-all:
  stage: destroy all
  image: 
    name: hashicorp/terraform:latest
    entrypoint:
      - '/usr/bin/env'
      - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
  before_script:
    - export AWS_ACCESS_KEY=${AWS_ACCESS_KEY_ID}
    - export AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY} 
  script:
    - cd terraform
    - terraform init
    - terraform destroy --auto-approve
  when: manual
  
